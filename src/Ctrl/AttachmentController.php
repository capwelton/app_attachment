<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Attachment\Ctrl;
use Capwelton\App\Attachment\Set\AttachmentSet;
use Capwelton\App\Attachment\Set\Attachment;

$App = app_App();
$App->includeRecordController();

/**
 * This controller manages actions that can be performed on attachments.
 * 
 * @method AttachmentSet getRecordSet()
 */
class AttachmentController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('Attachment'));
    }
    
    /**
     * @param int $id
     * @param boolean $inline
     */
    public function open($id, $inline = true)
    {
        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);
        
        $path = $recordSet->file->getFilePath($record);
        
        $basename = \bab_Path::decode($path->getBasename());
        
        if ($inline) {
            header('Content-Disposition: inline; filename="' . $basename . '"'."\n");
        } else {
            header('Content-Disposition: attachment; filename="' . $basename . '"'."\n");
        }
        
        $realPath = $path->getRealPath();
        $mime = bab_getFileMimeType($realPath);
        $fsize = filesize($realPath);
        header('Content-Type: ' . $mime . "\n");
        header('Content-Length: ' . $fsize . "\n");
        header('Content-transfert-encoding: binary'."\n");
        readfile($realPath);
        exit;
    }
    
    /**
     * Saves information (rank, type) about attached files.
     *
     * @param array     $data
     *
     * @return bool
     */
    public function saveAttachements($data = null)
    {
        $attachmentSet = $this->getRecordSet();
        
        $rank = 1;
        foreach ($data as $id => $file) {
            $attachment = $attachmentSet->get($id);
            $attachment->type = $file['type'];
            $attachment->rank = $rank;
            $attachment->save();
            $rank++;
        }
        
        $this->addReloadSelector('.depends-Attachment');
        return true;
    }
    
    public function saveFile($recordRef, $type = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $appC = $App->getComponentByName('Attachment');
        
        $attachmentSet = $this->getRecordSet();
        
        $record = $App->getRecordByRef($recordRef, true);
        if (!isset($record) || !$record->isReadable()) {
            throw new \app_AccessException($appC->translate('You do not have access to this page'));
        }
        
        $res = $W->FilePicker()->getTemporaryFiles('attachment');
        
        if (isset($res)) {
            foreach($res as $filePickerItem) {
                /*@var $filePickerItem \Widget_FilePickerItem */
                $attachement = $attachmentSet->newRecord();
                $attachement->reference = $recordRef;
                $attachement->file = $filePickerItem->getFilePath();
                if (isset($type)) {
                    $attachement->type = $type;
                }
                $attachement->save();
            }
        }
        return true;
    }
    
    
    
    /**
     *
     * @param string $recordRef
     * @param string $itemId
     * @throws \app_AccessException
     * @return \Widget_VBoxLayout
     */
    public function _attachments($recordRef, $thumbnailSize = 48, $itemId = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Attachment');
        $W = bab_Widgets();
        
        $record = $App->getRecordByRef($recordRef, true);
        if (!isset($record) || !$record->isReadable()) {
            throw new \app_AccessException($appC->translate('You do not have access to this page'));
        }
        
        $Ui = $App->Ui();
        
        $filePicker = $W->FilePicker();
        $filePicker->setName('attachment');
        $filePicker->oneFileMode(false);
        $filePicker->hideFiles();
        $filePicker->addClass('icon', 'widget-actionbutton', \Func_Icons::ACTIONS_LIST_ADD);
        
        $filePicker->setAjaxAction($this->proxy()->saveFile($recordRef));
        
        $filePicker->setSizePolicy('widget-toolbar section-button');
        
        $box = $W->VBoxLayout($itemId)->setVerticalSpacing(1, 'em');
        $box->setReloadAction($this->proxy()->_attachments($recordRef, $thumbnailSize, $box->getId()));
        $box->addClass('depends-Attachment');
        
        $box->addItem($filePicker);
        
        $view = $Ui->AttachmentList();
        $view->setThumbnailSize($thumbnailSize / 1.414, $thumbnailSize);
        
        $attachmentSet = $this->getRecordSet();
        $attachments = $attachmentSet->select($attachmentSet->reference->is($recordRef));
        $attachments->orderAsc($attachmentSet->rank);
        
        $view->setAttachments($attachments);
        $view->setSortable(true);
        $view->setAjaxAction($this->proxy()->saveAttachements(), '', 'change sortupdate');
        
        $box->addItem($view);
        
        $filePicker->setAssociatedDropTarget($view);
        
        return $box;
    }
    
    /**
     *
     * @param string $recordRef
     * @param string $itemId
     * @throws \app_AccessException
     * @return \Widget_VBoxLayout
     */
    public function _attachmentsList($recordRef, $thumbnailSize = 48, $itemId = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Attachment');
        $W = bab_Widgets();
        
        $record = $App->getRecordByRef($recordRef, true);
        if (!isset($record) || !$record->isReadable()) {
            throw new \app_AccessException($appC->translate('You do not have access to this page'));
        }
        
        $Ui = $App->Ui();
        
        $box = $W->VBoxLayout($itemId)->setVerticalSpacing(1, 'em');
        $box->setReloadAction($this->proxy()->_attachmentsList($recordRef, $thumbnailSize, $box->getId()));
        $box->addClass('depends-Attachment');
        
        $view = $Ui->AttachmentList();
        $view->setThumbnailSize($thumbnailSize / 1.414, $thumbnailSize);
        
        $attachmentSet = $this->getRecordSet();
        $attachments = $attachmentSet->select($attachmentSet->reference->is($recordRef));
        $attachments->orderAsc($attachmentSet->rank);
        
        $view->setAttachments($attachments);
        $view->setSortable(false);
        
        $box->addItem($view);
        
        return $box;
    }
    
    
    
    /**
     *
     * @param string $recordRef
     * @throws \app_AccessException
     * @return \app_Page
     */
    public function attachments($recordRef, $thumbnailSize = 48)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Attachment');
        $record = $App->getRecordByRef($recordRef, true);
        
        if (!isset($record) || !$record->isReadable()) {
            throw new \app_AccessException($appC->translate('You do not have access to this page'));
        }
        
        $page = $App->Ui()->Page();
        
        $view = $this->_attachments($recordRef, $thumbnailSize);
        $view->setSizePolicy('condensed');
        $page->setTitle($appC->translate('Attached files'));
        $page->addClass('col-md-12');
        
        $page->addItem($view);
        
        return $page;
    }
    
    public function delete($id)
    {
        parent::delete($id);        
        $this->addReloadSelector('.depends-Attachment');
        
        return true;
    }
}