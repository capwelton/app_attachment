<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com})
 */

namespace Capwelton\App\Attachment\Ui;

use Capwelton\App\Attachment\Set\Attachment;
use Capwelton\App\Attachment\Set\AttachmentSet;
use Capwelton\App\Attachment\Ctrl\AttachmentController;


class AttachmentList extends \app_UiObject
{
    /**
     * @var Attachment[]
     */
    protected $attachments;

    /**
     * @var int
     */
    protected $thumbnailWidth;

    /**
     * @var int
     */
    protected $thumbnailHeight;

    /**
     * @var string[]
     */
    protected $types;

    /**
     * @var bool
     */
    protected $sortable;
    
    /**
     * @var AttachmentController 
     */
    protected $ctrl;
    
    /**
     * @var AttachmentSet
     */
    protected $set;
    
    protected $attachmentComponent = null;

    /**
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App)
    {
        parent::__construct($App);

        $this->ctrl = $App->Controller()->Attachment();
        $this->set = $App->AttachmentSet();
        $this->attachmentComponent = $App->getComponentByName('Attachment');
        
        $W = bab_Widgets();

        $this->setThumbnailSize(96, 128);

        $this->setInheritedItem(
            $W->Form()
                ->addClass('widget-no-close')
                ->addClass('widget-100pc')
                ->setLayout($W->VBoxLayout())
                ->setName('data')
        );
    }



    /**
     * @return Attachment[]
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param Attachment[] $attachments
     * @return self
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;
        return $this;
    }


    /**
     * @return string[]
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @param string[]  $types
     * @return self
     */
    public function setTypes($types)
    {
        $this->types = $types;
        return $this;
    }



    /**
     * @return boolean
     */
    public function isSortable()
    {
        return $this->sortable;
    }

    /**
     * @param boolean $sortable
     * @return self
     */
    public function setSortable($sortable)
    {
        $this->sortable = $sortable;
        return $this;
    }


    public function setThumbnailSize($width, $height)
    {
        $this->thumbnailWidth = $width;
        $this->thumbnailHeight = $height;
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see \app_UiObject::display()
     */
    public function display(\Widget_Canvas $canvas)
    {
        $App = $this->App();
        $W = bab_Widgets();

        $this->addClass(\Func_Icons::ICON_LEFT_16);

        $attachments = $this->getAttachments();

        $isSortable = $this->isSortable() && ($attachments->count() > 1);


        if ($attachments->count() <= 0) {
            $this->addItem(
                $W->Html(bab_ToHtml($this->attachmentComponent->translate('No attached documents yet')))
                    ->addClass('alert')
            );
        }
        if ($isSortable) {
            $this->getLayout()->sortable(true);
        }

        foreach ($attachments as $attachment) {
            $attachementItem = $this->getAttachmentItem($attachment);
            $attachementItem->setSizePolicy('widget-list-element widget-actions-target');
            $this->addItem($attachementItem);
        }

        return parent::display($canvas);
    }
    
    public function getAttachmentItem(Attachment $attachment)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $types = $this->getTypes();
        $attachmentSet = $this->set;
        $nbTypes = count($types);
        
        if ($nbTypes <= 1) {
            $selectableOptions = array();
            $typeSelect = $W->Hidden();
            foreach ($types as $typeId => $type) {
                $selectableOptions[$typeId] = $type;
            }
        } else {
            $selectableOptions = array('' => '');
            $typeSelect = $W->Select();
            $typeSelect->addOption('', '');
            foreach ($types as $typeId => $type) {
                $typeSelect->addOption($typeId, $type);
                $selectableOptions[$typeId] = $type;
            }
        }
        
        $file = $attachmentSet->file->getFilePath($attachment);
        $filename = \bab_Path::decode($file->getBasename());
        
        return $W->HBoxItems(
            $W->Link(
                $W->ImageThumbnail($file)
                ->setThumbnailSize($this->thumbnailWidth, $this->thumbnailHeight)
                ->addClass('widget-bordered-thumbnail'),
                $this->ctrl->open($attachment->id)
            )->setOpenMode(\Widget_Link::OPEN_POPUP)
            ->setCanvasOptions(\Widget_Item::options()->minWidth($this->thumbnailWidth, 'px'))
            ->setSizePolicy('widget-1px'),
            $W->VBoxItems(
                $W->Link(
                    $filename,
                    $this->ctrl->open($attachment->id)
                )->setOpenMode(\Widget_Link::OPEN_POPUP),
                $typeSelect
                ->setName(array($attachment->id, 'type'))
                ->setValue('' . $attachment->type)
            )->setSizePolicy('widget-90pc'),
            $W->HBoxItems(
                $W->Link(
                    '',
                    $this->ctrl->open($attachment->id, false)
                )->addClass('icon', \Func_Icons::ACTIONS_DOCUMENT_DOWNLOAD)
                ->setTitle(sprintf($this->attachmentComponent->translate('Download file %s'), $filename)),
                $W->Link(
                    '',
                    $this->ctrl->confirmDelete($attachment->id)
                )->addClass('icon', \Func_Icons::ACTIONS_EDIT_DELETE)
                ->setOpenMode(\Widget_Link::OPEN_DIALOG),
                $W->Hidden()
                ->setName(array($attachment->id, 'id'))
                ->setValue($attachment->id)
            )->addClass('widget-actions', 'widget-align-right')
            ->setSizePolicy('widget-20pc')
        )->setVerticalAlign('middle')
        ->setHorizontalSpacing(1, 'em');
    }
}
