<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */
namespace Capwelton\App\Attachment\Set;

use Capwelton\App\Attachment\Set\Attachment;

/**
 * @property \ORM_FileField         $file
 * @property \ORM_StringField       $type
 * @property \ORM_IntField          $rank
 * @property \ORM_StringField       $reference
 * 
 * @method  \Func_App                   App()
 * @method  Attachment                  newRecord()
 * @method  Attachment                  get()
 * @method  Attachment                  request()
 * @method  Attachment[]|\ORM_Iterator  select(\ORM_Criteria $criteria)
 */
class AttachmentSet extends \app_TraceableRecordSet
{
    /**
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $App = $this->App();
        $this->setTableName($App->classPrefix.'Attachment');
        
        $this->setDescription($App->translatable('Attachment', 'Attachments'));
        
        $this->setPrimaryKey('id');
        
        $this->addFields(
            ORM_FileField('file')
            ->setDescription('File'),
            ORM_StringField('type')
            ->setDescription('Type'),
            ORM_IntField('rank')
            ->setDescription('Rank'),
            ORM_StringField('reference')
            ->setDescription('Reference')
        );
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new AttachmentBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new AttachmentAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return true;
    }
    
    
    /**
     * Returns a criterion matching records readable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isReadable()
    {
        return $this->all();
    }
    
    
    /**
     * Returns a criterion matching records updatable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isUpdatable()
    {
        return $this->all();
    }
    
    /**
     * Returns a criterion matching records deletable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isDeletable()
    {
        return $this->all();
    }
}

class AttachmentBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class AttachmentAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}