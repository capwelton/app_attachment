<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */
namespace Capwelton\App\Attachment\Set;

use Capwelton\App\Attachment\Ctrl\AttachmentController;

/**
 *
 * @property \string     $file
 * @property \string     $type
 * @property \int        $rank
 * @property \string     $reference
 * 
 * @method  \Func_App    App()
 */
class Attachment extends \app_TraceableRecord
{
    /**
     * {@inheritDoc}
     * @see \ORM_Record::getRecordTitle()
     */
    public function getRecordTitle()
    {
        if (! $this->file instanceof \bab_Path) {
            return \bab_Path::decode($this->file);
        }
        return \bab_Path::decode($this->file->getBasename());
    }
    
    public function isImage()
    {
        $App = $this->App();
        /* @var $set AttachmentSet */
        $set = $App->AttachmentSet();
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        
        $filePath = $set->file->getFilePath($this);
        $fileRealPath = $filePath->getRealPath();
        $mimetype = finfo_file($finfo, $fileRealPath);
        $imageTypes = array('image/jpg', 'image/jpeg', 'image/gif', 'image/png', 'image/bmp', 'image/tiff', 'image/webp');
        if(!in_array($mimetype, $imageTypes)){
            return false;
        }
        return true;
    }
    
    public function getImageDisplayableUrl()
    {
        $T = \bab_functionality::get('Thumbnailer', false);
        if (!$T) {
            return null;
        }
        
        /*@var $T Func_Thumbnailer */
        
        if(!$this->isImage()){
            return null;
        }
        
        
        $App = $this->App();
        /* @var $set AttachmentSet */
        $set = $App->AttachmentSet();
        
        $filePath = $set->file->getFilePath($this);
        $fileRealPath = $filePath->getRealPath();
        
        list($width, $height) = getimagesize($fileRealPath);
        
        $icon = $T->getIcon($fileRealPath, $width, $height);
        
        if (is_object($icon)) {
            $icon = $icon->__toString();
        }
        
        return $icon;
    }
    
    /**
     * @return NULL|Widget_Image
     */
    public function getImageDisplayable()
    {
        if(!$this->isImage()){
            return null;
        }
        
        $W = bab_Widgets();
        return $W->Image($this->getImageDisplayableUrl());
    }
    
    public function getBase64()
    {
        $T = \bab_functionality::get('Thumbnailer', false);
        if (!$T) {
            return null;
        }
        
        /*@var $T Func_Thumbnailer */
        
        if(!$this->isImage()){
            return null;
        }
        
        
        $App = $this->App();
        /* @var $set AttachmentSet */
        $set = $App->AttachmentSet();
        
        $filePath = $set->file->getFilePath($this);
        $fileRealPath = $filePath->getRealPath();
        
        $type = pathinfo($fileRealPath, PATHINFO_EXTENSION);
        $data = file_get_contents($fileRealPath);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        
        return $base64;
    }
}
